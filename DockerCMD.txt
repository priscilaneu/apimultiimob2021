rm -rf dist
yarn run sucrase ./src -d ./dist --transforms imports
docker build -t multiimob/node-web-app .
docker stop multiimob-api || true && docker rm multiimob-api || true && docker run --name multiimob-api --network=MULTIIMOB -p 8080:3333 -d multiimob/node-web-app
