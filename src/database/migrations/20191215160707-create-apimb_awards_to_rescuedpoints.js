'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'rescued_points',
      'award_id',
      {
        type: Sequelize.INTEGER,
        references: { model: 'awards', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true,
      });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('rescued_points', 'award_id');
  }
};
