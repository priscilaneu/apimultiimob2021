import Sequelize from 'sequelize';

import User from '../app/models/User';
import Broker from '../app/models/Broker';
import File from '../app/models/File';
import Award from '../app/models/Award';
import Recommendation from '../app/models/Recommendation';
import RescuedPoint from '../app/models/RescuedPoint';

import databaseConfig from '../config/database';

const models = [User, File, Broker, Award, Recommendation, RescuedPoint];

class Database{
    constructor(){
        this.init();
    }

    init(){
        this.connection = new Sequelize(databaseConfig);

        models
            .map(model => model.init(this.connection))
            .map(model => model.associate && model.associate(this.connection.models));
    }
}

export default new Database();
