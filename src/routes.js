import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import UserController from './app/controllers/UserController';
import UsersController from './app/controllers/UsersController';
import BrokerController from './app/controllers/BrokerController';
import BrokersController from './app/controllers/BrokersController';
import ClientController from './app/controllers/ClientController';
import ClientsController from './app/controllers/ClientsController';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import AwardController from './app/controllers/AwardController';
import AwardsController from './app/controllers/AwardsController';


import authMiddleware from './app/middlewares/auth';
import RecommendationController from "./app/controllers/RecommendationController";
import RecommendationsController from "./app/controllers/RecommendationsController";
import PointsBrokerController from "./app/controllers/PointsBrokerController";
import RescuedPointsController from "./app/controllers/RescuedPointsController";
import RescuedPointController from "./app/controllers/RescuedPointController";

const routes = new Router();
const upload = multer(multerConfig);

routes.post('/user', UserController.store);
routes.post('/sessions', SessionController.store);
routes.post('/sessions/client', SessionController.storeClient);
routes.get('/users', UsersController.index);
routes.post('/corretor', BrokerController.store);
routes.put('/corretor', BrokerController.update);
routes.get('/corretores', BrokersController.index);

routes.put('/client', ClientController.update);
routes.post('/client', ClientController.store);
routes.get('/clients', ClientsController.index);

routes.post('/award', AwardController.store);
routes.put('/award', AwardController.update);
routes.get('/awards', AwardsController.index);

routes.use(authMiddleware);

routes.put('/user', UserController.update);

routes.post('/files', upload.single('file'), FileController.store);

routes.post('/recommendation', RecommendationController.store);
routes.put('/recommendation', RecommendationController.update);
routes.get('/recommendations', RecommendationsController.index);
routes.get('/recommendations/points/', PointsBrokerController.points);
routes.get('/recommendations/in-negociation/', PointsBrokerController.pointsInNegociation);

routes.get('/rescued-points/', RescuedPointsController.pointsRescued);
routes.post('/rescued-point/', RescuedPointController.store);
routes.get('/rescued-points/awards/', RescuedPointsController.awardsRescued);



export default routes;
