export default{
    host: '',
    port: '',
    secure: false,
    auth: {
        user: '',
        pass: '',
    },
    default: {
        from: 'MultiImob App <noreply@gobarber.com>'
    },
};