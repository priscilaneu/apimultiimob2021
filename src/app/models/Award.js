import Sequelize, { Model } from 'sequelize';

class Award extends Model {
    static init(sequelize) {
        super.init({
            title: Sequelize.STRING,
            local: Sequelize.STRING,
            number_points: Sequelize.INTEGER,
            description: Sequelize.STRING,
            site: Sequelize.STRING
        },
            {
                sequelize,
            }
        );

        return this;
    }

    static associate(models) {
        this.belongsTo(models.File, { foreignKey: 'featured_image_id', as: 'featured_image' });
    }
}

export default Award;