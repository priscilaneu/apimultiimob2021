import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Recommendation extends Model {
    static init(sequelize){
        super.init({
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            phone: Sequelize.STRING,
            recommendation_type: Sequelize.STRING,
            status: Sequelize.STRING,
            number_points: Sequelize.INTEGER,
            obs: Sequelize.STRING
        },
        {
            sequelize,
        }
        );

        return this;
    }

    static associate(models){
        this.belongsTo(models.Broker, { foreignKey: 'broker_id', as: 'broker' });
        // this.belongsTo(models.Client, { foreignKey: 'client_id', as: 'client' });
    }
}

export default Recommendation;
