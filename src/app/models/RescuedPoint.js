import Sequelize, { Model } from 'sequelize';

class RescuedPoint extends Model {
    static init(sequelize) {
        super.init({
            broker_id: Sequelize.INTEGER,
            award_id: Sequelize.INTEGER
        },
            {
                sequelize,
            }
        );

        return this;
    }

    static associate(models){
        this.belongsTo(models.Broker, { foreignKey: 'broker_id', as: 'broker' });
        this.belongsTo(models.Award, { foreignKey: 'award_id', as: 'award' });
    }
}

export default RescuedPoint;
