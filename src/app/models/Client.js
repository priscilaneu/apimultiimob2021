import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Client extends Model {
    static init(sequelize){
        super.init({
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            phone: Sequelize.STRING,
            doc: Sequelize.STRING,
            password: Sequelize.VIRTUAL,
            password_hash: Sequelize.STRING,
            active: Sequelize.BOOLEAN,
            provider: Sequelize.BOOLEAN,
            obs: Sequelize.STRING
        },
        {
            sequelize,
        }
        );

        this.addHook('beforeSave', async (clients) => {
           if(clients.password){
                clients.password_hash = await bcrypt.hash(clients.password, 8);
           }
        });

        return this;
    }

    // static associate(models){
    //      this.belongsTo(models.File, { foreignKey: 'avatar_id', as: 'avatar' });
    // }

    checkPassword(password){
        return bcrypt.compare(password, this.password_hash);
    }
}

export default Client;
