import * as Yup from 'yup';
import Recommendation from "../models/Recommendation";

class RecommendationController {
    async store(req, res){
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            phone: Yup.string().required(),
            recommendation_type: Yup.string().required(),
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const {id, name, email, phone, recommendation_type, obs} = await Recommendation.create(req.body);

        return res.json({id, name, email, phone, recommendation_type, obs});
    }

    async update(req, res){
        const schema = Yup.object().shape({
            name: Yup.string(),
            email: Yup.string().email(),
            phone: Yup.string().required(),
            recommendation_type: Yup.string().required(),
        });

        req.body.recommendation_type = req.body.recommendation_type.value
        req.body.status = req.body.status ? req.body.status.value : null

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }
        
        const recommendation = await Recommendation.findByPk(req.body.id);

        await recommendation.update(req.body);

        const {id, name, email, phone, recommendation_type, number_points, status, obs} =  await Recommendation.findByPk(req.body.id)

        return res.json({id, name, email, phone, recommendation_type, number_points, status, obs});

    }
}

export default new RecommendationController();