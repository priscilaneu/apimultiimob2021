import Award from "../models/Award";
import File from '../models/File';

class AwardsController{
    async index(req, res){
        const awards = await Award.findAll({
            attributes: ['id', 'title', 'local', 'number_points', 'description', 'site'],
            include: [
                 {
                     model: File,
                     as: 'featured_image',
                     attributes: ['name', 'path', 'url'],
                 },
             ],
        });
        return res.json(awards);
    }
}

export default new AwardsController();
