import Award from "../models/Award";
import RescuedPoint from "../models/RescuedPoint";
import File from "../models/File";

class RescuedPointsController{
    async pointsRescued(req, res){
        const rescuedPoints = await RescuedPoint.findAll({
            where: { broker_id: req.query.id  },
            include: [
                {
                    model: Award,
                    as: 'award',
                    attributes: ['number_points'],
                }
             ],
        });
        let points = 0
        if (rescuedPoints) {
            rescuedPoints.forEach(function (rescuedPoint) {
                points += rescuedPoint.award.number_points ? rescuedPoint.award.number_points : 0
            })
        }
        return res.json(points);
    }

    async awardsRescued(req, res){
        const rescuedPoints = await RescuedPoint.findAll({
            where: { broker_id: req.query.id  },
            include: [
                {
                    model: Award,
                    as: 'award',
                    attributes: ['title', 'local', 'number_points', 'description', 'site'],
                    include: [
                        {
                            model: File,
                            as: 'featured_image',
                            attributes: ['name', 'path', 'url'],
                        },
                    ]
                }
            ],
        });
        const awards = rescuedPoints.map(function (rescuedPoint) {
            return rescuedPoint.award
        })
        return res.json(awards);
    }
}

export default new RescuedPointsController();
