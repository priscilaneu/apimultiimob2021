import * as Yup from 'yup';
import Client from '../models/Client';
// import File from '../models/File';
import {Op} from "sequelize";

class ClientController {
    async store(req, res){
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            phone: Yup.string().required(),
            password: Yup.string().required().min(6),
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const clientExists = await Client.findOne({ where: {
                [Op.or]: [
                    { email: req.body.email },
                    { phone: req.body.phone },
                ]
            }});

        if(clientExists){
            return res.status(400).json({ error: 'Client already exists' });
        }

        req.body.active = true;
        req.body.provider = false;
        const {id, name, email, phone, doc, password, active, provider, obs} = await Client.create(req.body);

        return res.json({id, name, email, phone, doc, password, active, provider, obs});
    }

    async update(req, res){
        const schema = Yup.object().shape({
            name: Yup.string(),
            email: Yup.string().email(),
            oldPassword: Yup.string().min(6),
            password: Yup.string()
                .min(6)
                .when('oldPassword', (oldPassword, field) =>
                    oldPassword ? field.required() : field
            ),
            confirmPassword: Yup.string().when('password', (password, field) =>
                password ? field.required().oneOf([Yup.ref('password')]) : field
            )
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const { email, oldPassword} = req.body;
        const client = await Client.findByPk(req.body.id);

        if(email !== client.email){
            const clientExists = await Client.findOne({ where: {email: req.body.email} });

            if(clientExists){
                return res.status(400).json({ error: 'Client already exists' });
            }
        }

        if(oldPassword && !(await client.checkPassword(oldPassword))){
            return res.status(401).json({ error: 'Password does not match' });
        }

        await client.update(req.body);

        // const {id, name, phone, doc, active, provider, obs} =  await Broker.findByPk(req.body.id, {
        //     include: [
        //          {
        //              model: File,
        //              as: 'avatar',
        //              attributes: ['id', 'path', 'url'],
        //          }
        //      ]
        // })

        return res.json({id, name, email, phone, doc, active, provider, obs});

    }
}

export default new ClientController();
