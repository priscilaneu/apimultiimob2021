import Client from "../models/Client";

class ClientsController{
    async index(req, res){
        const clients = await Client.findAll({
            where: { provider: false },
            attributes: ['id', 'name', 'email', 'phone', 'doc', 'obs'],
        });

        return res.json(clients);
    }
}

export default new ClientsController();
