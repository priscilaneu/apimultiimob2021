import User from "../models/User";

class UsersController{
    async index(req, res){
        const users = await User.findAll({
            where: { provider: false },
            attributes: ['id', 'name', 'email', 'phone', 'doc', 'obs'],
        });

        return res.json(users);
    }
}

export default new UsersController();