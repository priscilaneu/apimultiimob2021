import Broker from "../models/Broker";

class BrokersController{
    async index(req, res){
        const brokers = await Broker.findAll({
            where: { provider: false },
            attributes: ['id', 'name', 'email', 'phone', 'doc', 'obs'],
        });

        return res.json(brokers);
    }
}

export default new BrokersController();
