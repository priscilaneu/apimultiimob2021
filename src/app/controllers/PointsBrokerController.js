import User from "../models/User";
import File from '../models/File';
import Recommendation from "../models/Recommendation";

class PointsBrokerController{
    async points(req, res){
        const recommendations = await Recommendation.findAll({
            where: { broker_id: req.query.id, status: 'Vendido'  }
        });
        let points = 0
        if (recommendations) {
            recommendations.forEach(function (recommendation) {
                points += recommendation.number_points ? recommendation.number_points : 0
            })
        }
        return res.json(points);
    }

    async pointsInNegociation(req, res){
        const recommendations = await Recommendation.findAll({
            where: { broker_id: req.query.id, status: 'Negociacao'  }
        });
        let points = 0
        if (recommendations) {
            recommendations.forEach(function (recommendation) {
                points += recommendation.number_points ? recommendation.number_points : 0
            })
        }
        return res.json(points);
    }
}

export default new PointsBrokerController();
