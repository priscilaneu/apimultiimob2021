import * as Yup from 'yup';
import Award from '../models/Award';
import File from '../models/File';
import RescuedPoint from "../models/RescuedPoint";

class RescuedPointController {
    async store(req, res){
        const rescuedPointExists = await RescuedPoint.findOne({ where: { award_id: req.body.award_id
                ,  broker_id: req.body.broker_id} });

        if(rescuedPointExists){
            return res.status(400).json({ error: 'Rescued already exists' });
        }
        const {id, broker_id, award_id} = await RescuedPoint.create(req.body);

        return res.json({id, broker_id, award_id});
    }
}

export default new RescuedPointController();
