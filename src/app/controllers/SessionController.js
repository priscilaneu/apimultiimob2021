import jwt from 'jsonwebtoken';
import * as Yup from 'yup';

import User from '../models/User';
// import File from '../models/File';
import authConfig from '../../config/auth';
import bcrypt from 'bcryptjs';
import Client from '../models/Client';

class SessionController{
    async store(req, res){
        const schema = Yup.object().shape({
            email: Yup.string().email().required(),
            password: Yup.string().required(),
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const { email, password } = req.body;
        const user = await User.findOne({ 
            where: {email} ,
            // include: [
            //     {
            //         model: File,
            //         as: 'avatar',
            //         attributes: ['id', 'path', 'url'],
            //     }
            // ]
        });
        if(!user){
            return res.status(401).json({ error: 'User not found' });
        }

        if(!(await user.checkPassword(password))){
            return res.status(401).json({ error: 'Password does not match' });
        }
        const { id, name, phone, doc, active, obs } = user;

        return res.json({
            user: {
                id, name, email, phone, doc, active, obs
            },
            token: jwt.sign({ id }, authConfig.secret, {
                expiresIn: authConfig.expiresIn,
            })
        })
    }

    async storeClient(req, res){
        const schema = Yup.object().shape({
            email: Yup.string().email().required(),
            password: Yup.string().required(),
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const { email, password } = req.body;
        const client = await Client.findOne({ 
            where: {email} ,
            // include: [
            //     {
            //         model: File,
            //         as: 'avatar',
            //         attributes: ['id', 'path', 'url'],
            //     }
            // ]
        });
        if(!client){
            return res.status(401).json({ error: 'Client not found' });
        }

        if(!(await client.checkPassword(password))){
            return res.status(401).json({ error: 'Password does not match' });
        }
        const { id, name, phone, doc, active, obs } = client;

        return res.json({
            client: {
                id, name, email, phone, doc, active, obs
            },
            token: jwt.sign({ id }, authConfig.secret, {
                expiresIn: authConfig.expiresIn,
            })
        })
    }
}

export default new SessionController();