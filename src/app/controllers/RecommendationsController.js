import Broker from "../models/Broker";
import Recommendation from "../models/Recommendation";

class RecommendationsController{
    async index(req, res){
        const recommendations = await Recommendation.findAll({
            attributes: ['id', 'name', 'email', 'phone', 'recommendation_type', 'number_points', 'status', 'obs'],
            include: [
                {
                    model: Broker,
                    as: 'broker',
                    attributes: ['id', 'name'],
                },
            ]
        });

        return res.json(recommendations);
    }
}

export default new RecommendationsController();