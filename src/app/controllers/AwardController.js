import * as Yup from 'yup';
import Award from '../models/Award';
import File from '../models/File';

class AwardController {
    async store(req, res){
        const schema = Yup.object().shape({
            title: Yup.string().required(),
            local: Yup.string().required(),
            number_points: Yup.number().required(),
            site: Yup.string().required(),
        });
        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const awardExists = await Award.findOne({ where: { title: req.body.title } });

        if(awardExists){
            return res.status(400).json({ error: 'Award already exists' });
        }
        const {id, title, local, number_points, description, site} = await Award.create(req.body);

        return res.json({id, title, local, number_points, description, site});
    }

    async update(req, res){
        const schema = Yup.object().shape({
            title: Yup.string(),
            local: Yup.string(),
            number_points: Yup.number(),
            site: Yup.string(),
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }
        
        const award = await Award.findByPk(req.body.id);

        if(req.body.title != award.title){
            const awardExists = await Award.findOne({ where: {title: req.body.title} });

            if(awardExists){
                return res.status(400).json({ error: 'Award already exists' });
            }
        }

        await award.update(req.body);

        const {id, title, local, number_points, description, site} =  await Award.findByPk(req.body.id, {
            include: [
                 {
                     model: File,
                     as: 'featured_image',
                     attributes: ['id', 'path', 'url'],
                 }
             ]
        })

        return res.json({id, title, local, number_points, description, site});

    }
}

export default new AwardController();