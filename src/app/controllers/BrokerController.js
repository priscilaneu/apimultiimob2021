import * as Yup from 'yup';
import Broker from '../models/Broker';
// import File from '../models/File';
import {Op} from "sequelize";

class BrokerController {
    async store(req, res){
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            phone: Yup.string().required(),
            password: Yup.string().required().min(6),
            phone: Yup.string().required(),
            doc: Yup.string().required().min(11),
        });

        if(!(await schema.name.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const brokerExists = await Broker.findOne({ where: {
                [Op.or]: [
                    { email: req.body.email },
                    { phone: req.body.phone },
                    { doc: req.body.doc }
                ]
            }});

        if(brokerExists){
            return res.status(400).json({ error: 'Broker already exists' });
        }

        req.body.active = true;
        req.body.provider = false;
        const {id, name, email, phone, doc, password, active, provider, obs} = await Broker.create(req.body);

        return res.json({id, name, email, phone, doc, password, active, provider, obs});
    }

    async update(req, res){
        const schema = Yup.object().shape({
            name: Yup.string(),
            email: Yup.string().email(),
            oldPassword: Yup.string().min(6),
            password: Yup.string()
                .min(6)
                .when('oldPassword', (oldPassword, field) =>
                    oldPassword ? field.required() : field
            ),
            confirmPassword: Yup.string().when('password', (password, field) =>
                password ? field.required().oneOf([Yup.ref('password')]) : field
            )
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation fails' });
        }

        const { email, oldPassword} = req.body;
        const broker = await Broker.findByPk(req.body.id);

        if(email !== broker.email){
            const brokerExists = await Broker.findOne({ where: {email: req.body.email} });

            if(brokerExists){
                return res.status(400).json({ error: 'Broker already exists' });
            }
        }

        if(oldPassword && !(await broker.checkPassword(oldPassword))){
            return res.status(401).json({ error: 'Password does not match' });
        }

        await broker.update(req.body);

        // const {id, name, phone, doc, active, provider, obs} =  await Broker.findByPk(req.body.id, {
        //     include: [
        //          {
        //              model: File,
        //              as: 'avatar',
        //              attributes: ['id', 'path', 'url'],
        //          }
        //      ]
        // })

        return res.json({id, name, email, phone, doc, active, provider, obs});

    }
}

export default new BrokerController();
